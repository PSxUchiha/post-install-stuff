#!/bin/bash

# Install a few essential packages

sudo pacman -S --noconfirm --needed vim telegram-desktop gnome-disk-utility mpv zsh steam plasma-browser-integration
yay -S --noconfirm --needed discord spotify spicetify-cli qbittorrent-qt5
yay -S flatpak
flatpak install com.obsproject.Studio/x86_64/stable


# Fix permissions for spotify binary

sudo chmod a+wr /opt/spotify
sudo chmod a+wr /opt/spotify/Apps -R

# Run Spotify for the first time

spotify

# Clone the spicetify repo into a git folder to keep things tidy

mkdir git
cd git
git clone https://github.com/morpheusthewhite/spicetify-themes.git

# Install the theme Nord-Dark, can replace it with others too

cd spicetify-themes
cp -r * ~/.config/spicetify/Themes
cd "$(dirname "$(spicetify -c)")/Themes/Dribbblish"
mkdir -p ../../Extensions
cp dribbblish.js ../../Extensions/.
spicetify config extensions dribbblish.js
spicetify config current_theme Dribbblish color_scheme Nord-Dark
spicetify config inject_css 1 replace_colors 1 overwrite_assets 1
spicetify backup apply

# Installation of ohmyzsh

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"


